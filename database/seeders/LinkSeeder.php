<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LinkSeeder extends Seeder
{
	public function run()
	{
		$userId = intval(DB::table('users')->where('email', 'aaa@aaa.aaa')->first()->id);

		if ($userId) {
			for ($i = 0; $i < 10; $i++) {
				DB::table('links')->insert([
					'original_url' => 'https://example.com/' . Str::random(10),
					'short_url' => Str::random(6),
					'user_id' => $userId,
					'created_at' => now(),
					'updated_at' => now(),
				]);
			}
		}
	}
}
