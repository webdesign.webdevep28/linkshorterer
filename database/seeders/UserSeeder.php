<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
	public function run()
	{
		DB::table('users')->insert([
			'name' => 'aaa@aaa.aaa',
			'email' => 'aaa@aaa.aaa',
			'email_verified_at' => now(),
			'password' => Hash::make('aaa@aaa.aaa'),
			'remember_token' => Str::random(10),
		]);
	}
}
