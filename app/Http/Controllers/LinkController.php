<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLinkRequest;
use App\Models\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class LinkController extends Controller
{
// Создание короткой ссылки
	public function store(StoreLinkRequest $request)
	{

		$rules = [
			'original_url' => 'required|url|max:65',
		];

		$messages = [
			'original_url.required' => 'Поле URL обязательно для заполнения.',
			'original_url.url' => 'Некорректный формат URL.',
			'original_url.max' => 'URL слишком длинный. Максимальная длина 2048 символов.',
		];

		$request->validate($rules, $messages);

		do {
			$shortUrl = Str::random(6);
		} while (Link::where('short_url', $shortUrl)->exists());

		$link = Link::create([
			'original_url' => $request->original_url,
			'short_url' => Str::random(6)
		]);

		return response()->json($link);
	}

// Получение списка всех ссылок
	public function index()
	{
		$links = Link::all();
		return response()->json($links);
	}

// Обновление информации о ссылке
	public function update(Request $request, $id)
	{
		$rules = [
			'original_url' => 'required|url|max:2048',
		];

		$messages = [
			'original_url.required' => 'Поле URL обязательно для заполнения.',
			'original_url.url' => 'Некорректный формат URL.',
			'original_url.max' => 'URL слишком длинный. Максимальная длина 2048 символов.',
		];

		$request->validate($rules, $messages);

		$link = Link::findOrFail($id);
		$link->update($request->all());

		return response()->json($link);
	}

// Удаление ссылки
	public function destroy($id)
	{
		$link = Link::findOrFail($id);
		$link->delete();

		return response()->json('Link deleted successfully');
	}

// Переход по короткой ссылке
	public function redirect($shortUrl)
	{
		$link = Link::where('short_url', $shortUrl)->firstOrFail();
		$link->increment('clicks'); // Увеличиваем счетчик кликов

		return Redirect::to($link->original_url);
	}
}