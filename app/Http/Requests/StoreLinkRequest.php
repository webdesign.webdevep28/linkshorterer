<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreLinkRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array<string, ValidationRule|array<mixed>|string>
	 */
	public function rules(): array
	{
		return [
			//
		];
	}

	public function store(Request $request)
	{
		$input = $request->all();

		// Else one sanitizing example using here:
		$input['link'] = htmlspecialchars($input['link']);

		$validated = $request->validate([
			'link' => 'required|string|max:255',
		]);

		// Обработка данных...
	}

}

